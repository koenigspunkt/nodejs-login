const redisClient = require("../utils/redis").redisClient;

exports.renderIndex = renderIndex;
function renderIndex(req, res) {
  return res.render("index", { title: "Express" });
}
