const config = require("../../config").Config;
let redisClient;
const redis = require("redis");
const Promise = require("bluebird");

Promise.promisifyAll(redis.RedisClient.prototype);

redisClient = redis.createClient(config.redis.ip);
redisClient.on("error", function(err) {
  console.log("fatal", "redis default connection error: " + err);
  throw err;
});

redisClient.on("connect", function() {
  console.log("Redis is ready");
});
redisClient = Promise.promisifyAll(redisClient);

exports.redis = redis;
exports.redisClient = redisClient;
