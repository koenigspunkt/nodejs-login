const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let UserSchema = new Schema(
  {
    created_at: { type: Date },
    updated_at: { type: Date },
    deleted: { type: Boolean }
  },
  { runSettersOnQuery: true }
);

let UserModel = mongoose.model("User", UserSchema);

module.exports = UserModel;
