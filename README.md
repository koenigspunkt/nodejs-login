##NodeJs Login Challenge

###Install Dev Enviroment

Have Docker installed.
https://www.docker.com/products/docker-desktop

```
cd development
docker-compose up -d
docker-compose exec login-node npm install
```

This will build multiple containers as well as installing the node modules.
Mongo and Redis are already connected. An incomplete example Model is places in `/src/utils/user`

You can access the logs of the container via:

```
docker-compose logs -f --tail=300 login-node
```

###Task

- [ ] create a register route
- [ ] complete the model for the user with email, lastname and firstname and birthday
- [ ] store a new user in the database
- [ ] create a route to login the user
- [ ] change the birthday of the logged in user
- [ ] create a logout route
