const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");

const config = require("./config").Config;
const bodyParser = require("body-parser");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();
const db = mongoose.connection;

db.on("connecting", function() {
  console.log("connecting to MongoDB...");
});
db.on("error", function(error) {
  console.error("Error in MongoDb connection: " + error);
  mongoose.disconnect();
});
db.on("connected", function() {
  console.log("MongoDB connected!");
});
db.once("open", function() {
  console.log("MongoDB connection opened!");
});
db.on("reconnected", function() {
  console.log("MongoDB reconnected!");
});
db.on("disconnected", function() {
  console.log("MongoDB disconnected!");
  mongoose.connect(config.mongodb.connectionString, {
    promiseLibrary: require("bluebird"),
    server: { auto_reconnect: true }
  });
});

// disable ugly header-setting
app.disable("x-powered-by");
// enable proxy
app.enable("trust-proxy");
// allow headers
app.use(function(req, res, next) {
  res.setHeader(
    "Access-Control-Allow-Origin",
    config.headersettings.alloworigin
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    config.headersettings.allowmethods
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    config.headersettings.allowheaders
  );

  // Manually calling of the Options Method Reqeuest to return OK, that it won't get
  // into the Routing and get access errrors
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }

  return next();
});

// Middleware

app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(cookieParser());

mongoose.connect(config.mongodb.connectionString, {
  promiseLibrary: require("bluebird"),
  auto_reconnect: true
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
