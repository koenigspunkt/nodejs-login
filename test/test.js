const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../app");

chai.use(chaiHttp);
chai.should();
describe("Users", () => {
  describe("GET /", () => {
        // Test to get single student record
    it("get users", done => {
      chai
      .request(app)
      .get(`/users/`)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
    });
  });
});