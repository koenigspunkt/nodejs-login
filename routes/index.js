const express = require("express");
const router = express.Router();
const userController = require("../src/controller/index");
/* GET home page. */
router.get("/", userController.renderIndex);

module.exports = router;
