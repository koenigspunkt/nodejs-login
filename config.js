const _ = require("lodash");
let generalConfig = {
  headersettings: {
    alloworigin: "*",
    allowmethods: "GET,POST,PUT,DELETE",
    allowheaders: "X-Requested-With,content-type,If-Modified-Since"
  },
  test: {
    url: "http://localhost:4242"
  }
};

const development = {
  development: true,
  port: 3000,
  mongodb: {
    connectionString: "mongodb://login-mongo/database"
  },
  redis: {
    ip: "redis://login-redis",
    port: "6379"
  }
};

console.log("Server Started. Set env to development");
_.merge(generalConfig, development);

exports.Config = generalConfig;
